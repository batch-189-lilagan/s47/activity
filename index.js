const inputFirstName = document.querySelector("#input-first-name");
const inputLastName = document.querySelector("#input-last-name");
const output = document.querySelector("#output");

const generateOutput = (e) => {
    output.innerHTML = `${inputFirstName.value} ${inputLastName.value}`;
};

inputFirstName.addEventListener('keyup', generateOutput);
inputLastName.addEventListener('keyup', generateOutput);

// Additional Test Feature
const button = document.querySelector("#reset");

button.addEventListener('click', (e) => {
    inputFirstName.value = '';
    inputLastName.value = '';
    output.innerHTML = '';
});



// For solution 2
// const outputFirstName = document.querySelector("#output-first-name")
// const outputLastName = document.querySelector("#output-last-name")

// const generateFirstName = (e) => {
//     outputFirstName.innerHTML = inputFirstName.value
// }
// const generateLastName = (e) => {
//     outputLastName.innerHTML = inputLastName.value
// }



